import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable, throwError } from 'rxjs'
import { catchError } from 'rxjs/operators'



@Injectable({
  providedIn: 'root'
})
export class StarWarsDataService {

  starWarsDataAPI:string = 'https://swapi.co/api/'


  constructor(private http:HttpClient) { }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      
    console.error(error);
    alert("There is no information on this option")
    return throwError(
      'There is no data available for this option')
    };
  }

  getData() {
    return this.http.get(`${this.starWarsDataAPI}users`).pipe(
      catchError(this.handleError<Object[]>('getStarWarsData', []))
    )
  }

  getStarWarsData(id, category) {
    return this.http.get(`${this.starWarsDataAPI}${category}/${id}`)
    .pipe(
      catchError(this.handleError<Object[]>('getStarWarsData', []))
    )
  }
}
