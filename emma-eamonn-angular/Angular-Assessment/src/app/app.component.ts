import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { StarWarsDataService } from './star-wars-data.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  users
  swId:number = 1
  whichCategory:string
  starWars:Object[]= [{cat:'people'},{cat:'planets'},{cat:'vehicles'},{cat:'species'},{cat:'starships'}]
  model
  whichImage:Object[]= [{cat:'https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwjR84OO4KXkAhX8VBUIHfjMAIEQjRx6BAgBEAQ&url=http%3A%2F%2Fwww.quickmeme.com%2Fmeme%2F3s53sm&psig=AOvVaw3viifEKfoAxzCjoTxCuTDr&ust=1567088012330022'}]

  constructor(private starwarsService: StarWarsDataService) {}
  
  handleClick(){
    this.starwarsService.getStarWarsData(this.swId, this.whichCategory)
    .subscribe( (result)=>{this.model=result})
  }


invokeService() {
  this.starwarsService.getData().subscribe( (result)=>{
    this.users=result
    console.log(result)})
  }

  ngOnInit(){
    this.invokeService()
  }
}
  



